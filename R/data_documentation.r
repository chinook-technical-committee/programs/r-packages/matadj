#' @title List of named data frames defining fisheries and stocks
#'
#' @description
#' @details
#' The data frame fishery.model.def has classification in terminal and pre-terminal by age. Source is Tim Dalton, CTC.
#' A dataset containing stock acronym mapping betweens ERA indicator stocks and old and new Chinook model stocks. Source is Gayle Brown (retired DFO), CTC
#' 
#'
#' @examples
#' names(defs)
#' str(defs$stockmap)
"defs"


