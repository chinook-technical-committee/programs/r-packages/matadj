# MATAEQadj
R package to do maturity adjustments using the Stock Aggregate Cohort Evaluation (SACE, Pronunciation: /ˈseɪs/) method developed by Tim Dalton and Chuck Parken.

 

> "The program... first it estimates abundance at sea (age specific cohorts) under the ERA/Model assumptions of natural mortality and the Gorilla Assumption of CWT recoveries representing the stock aggregate, then using cohorts-marine catch it estimates maturation rates (return/(cohort-catch))." - Tim Dalton

Code named, FIEAASUTEMAONMATGAOCWTRRTSATUCMCIEMR.

```
install.packages("remotes")
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/matadj.git", build_vignettes = TRUE, force = TRUE) 
```

Load `matadj` into memory:
```
require(matadj)
```

The function `writeScript` will make it easier to start things. The help file has an example:
```
?matadj::writeScript()
```

In the example we can see how to seek what demo scripts are available:
```
demo(package = "matadj")
```

To save and open a specific demo script (this one named "demo_sace2"):
```
writeScript("demo_sace2", package = "matadj")

```

