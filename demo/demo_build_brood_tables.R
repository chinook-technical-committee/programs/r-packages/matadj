
#currently this is a messy example of how to build brood table csv files from fcs data combined with several Cdn xls files (for age 2 data); and for importing Tim's xls file of US stocks.
#hopefully none of this will be needed for 2023 analysis year as we'll be working with 4 age FCS files.
#Michael FOlkes


# packages ----------------------------------------------------------------



require(ctctools)
require(matadj)



# setup -------------------------------------------------------------------


rm(list=ls())

settings <- yaml::read_yaml("user.settings.config")
settings

source.filepath <- paste(settings$path.data, "OCN2024_4age.xlsx", sep="/")
dest.filepath <- paste(settings$path.data, "OCN2024.FCS", sep="/")


# build brood tables ------------------------------------------------------

stocks <- c("BON", "CWF", "FHF", "FCF", "LGS", "LRW", "MCB", "MGS", "MOC", "NOC", "NSA", "SKG", "SPR", "SSA", "TST", "URB", "WCH", "WCN", "WSH", "WVH", "WVN")

data.fcs <- matadj::readFCS2(dest.filepath)

#extract just stock-specific data tables from the list:
data.stocks <- lapply(data.fcs$stocks, "[[", "data.fcs")
#subset to desired stocks:
data.stocks <- data.stocks[names(data.stocks) %in% stocks]

#stockmap identifies true start age by stock so that brood year can be calculated
stockmap <- getStockmap()

#Tim needs to update this csv file with annual estimates of Terminal harvest rate for NOC and MOC
#this is how to convert escapement to terminal return. New stocks can be added to this file if desired, and without having to change any code their escapement data will be converted to terminal return
hr <- read.csv(paste(settings$data.matadj.path, "terminalHR.csv", sep="/"), comment.char = "#", blank.lines.skip = TRUE)

#convert fcs data to a list of data frames that are brood year specific
data.brood <- convertFCStoBrood(data.stocks, stockmap = stockmap, hr=hr)
length(data.stocks)
length(data.brood)

#list of data frames in same format as needed by SACE2
data.brood$SSA

#write brood tables to individual csv files, eventually the csv files won't be needed by sace as it'll use the R list
writeBroodcsv(data.brood, outpath = settings$data.broodtables.path)



# main --------------------------------------------------------------------
#this is all old code and should not be needed.

# convertXLStoFCS(source.filepath, dest.filepath)
# 
# fcs.list <- matadj::readFCS2(dest.filepath)
# 
# 
# 
# data.fcs <- fcs.list$data.long
# str(data.fcs)
# unique(data.fcs$stock)
# #stocks.cdn <- c("WVN", "WVH", "LGS", "MGS")
# stocks <- c("BON", "CWF", "LGS", "LRW", "MCB", "MGS", "MOC", "NOC", "NSA", "SKG", "SPR", "SSA", "TST", "URB", "WCH", "WCN", "WSH", "WVH", "WVN")
# 
# data.fcs <- data.fcs[data.fcs$stock %in% stocks,]
# str(data.fcs)
# unique(data.fcs[,c("stock", "data.type", "age.structure")])
# dim(data.fcs)
# View(data.fcs)

# #this file updated by Max and found at:
# #\\dcbcpbsna01a\CTC_Files$\03_Catch_Escapement\Catch&Escapement_2021\Escapement\Term FP
# data.age2 <- as.data.frame(readxl::read_excel("C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2022/data/matadj/WCVI_2022.xlsx", sheet = "w_age 2", skip = 2))
# 
# wvh_age2 <- data.frame(stock="WVH", year= data.age2$Year, age2= data.age2$`2...2`, stringsAsFactors = FALSE)
# wvh_age2 <- wvh_age2[!is.na(wvh_age2$age2),]
# wvn_age2 <- data.frame(stock="WVN", year= data.age2$Year, age2= data.age2$`2...7`, stringsAsFactors = FALSE)
# wvn_age2 <- wvn_age2[!is.na(wvn_age2$age2),]
# 
# 
# data.lgs <- as.data.frame(readxl::read_excel("C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2022/data/matadj/LGS_Naturals_2022.xlsx", skip = 5))
# data.lgs$stock <- "LGS"
# data.lgs$age2 <- data.lgs$jacks
# data.lgs$year <- data.lgs$Year
# data.lgs <- data.lgs[!is.na(data.lgs$age2),]
# str(data.lgs)
# 
# 
# data.mgs <- as.data.frame(readxl::read_excel("C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/awg/calibration_model2_analyses/2022/data/matadj/PPS_MGS_EscapementData_2022.xlsx", sheet = "Middle Georgia Strait Data", range = "L59:M100", col_names = FALSE))
# colnames(data.mgs) <- c("year", "age2")
# data.mgs$stock <- "MGS"
# data.mgs <- data.mgs[!is.na(data.mgs$age2),]
# 
# common.colnames <- Reduce(intersect, list(colnames(wvh_age2), colnames(wvn_age2), colnames(data.mgs), colnames(data.lgs)))
# 
# data.age2 <- do.call("rbind", list(wvh_age2[,common.colnames], wvn_age2[,common.colnames], data.lgs[,common.colnames], data.mgs[,common.colnames]))
# str(data.age2)
# unique(data.age2[,c("stock")])
# 
# str(data.fcs)


# data.fcs.wide <- reshape(data.fcs, direction = 'wide', drop = c("data.type", "age.structure"), idvar = c("stock", "year"),  timevar = "agegroup")
# colnames(data.fcs.wide) <- gsub("value.", "", colnames(data.fcs.wide))
# colnames(data.fcs.wide) <- gsub("age.", "age", colnames(data.fcs.wide))
# View(data.fcs.wide)
# 
# data.merge <- merge(data.fcs.wide, data.age2, by=c("year", "stock"), all.x = TRUE)
# nrow(data.fcs.wide)
# nrow(data.merge)
# View(data.merge)
# str(data.merge)
# 
# 
# varying <- colnames(data.merge)[grep("age[0-9]", colnames(data.merge))]
# data.merge.long <- reshape(data.merge, drop = 'totalabundance', direction = 'long', idvar = c("year", "stock"), varying = list(varying), v.names = "value",timevar = "age", times = varying)
# data.merge.long$age <- gsub("age", "", data.merge.long$age)
# data.merge.long$age <- as.integer(data.merge.long$age)
# str(data.merge.long)
# data.merge.long$broodyear <- data.merge.long$year-data.merge.long$age
# data.merge.wide <- reshape(data.merge.long,drop = 'year', direction = 'wide', idvar =  c("broodyear", "stock"), timevar = 'age' )
# colnames(data.merge.wide) <- gsub("value.", "age", colnames(data.merge.wide))
# 
# data.merge.wide <- data.merge.wide[order(data.merge.wide$stock, data.merge.wide$broodyear),]
# View(data.merge.wide)
# 
# lapply(unique(data.merge.wide$stock), function(stock){
#   print(stock)
#   dat.tmp <- data.merge.wide[data.merge.wide$stock==stock,]
#   if(nrow(dat.tmp)>0){
#   dat.tmp$BY <- dat.tmp$broodyear
#   age.cols <- colnames(dat.tmp)[grep("age[0-9]", colnames(dat.tmp))]
#   dat.tmp <- dat.tmp[,c("BY", sort(age.cols))]
#   write.csv(dat.tmp, row.names = FALSE, quote = FALSE, file = paste0("../data/matadj/", stock, ".csv"))
#   }
# })


# data import: brood tables -----------------------------------------------

#path for Tim's xls file:
data.list <- extractBroodTables(xls.filename = "Skagit TR esc SUS AK brood tables 3 21 22.xlsx")
lapply(data.list, names)
length(data.list)


# data export: brood table to csv -----------------------------------------
# this is a backup method, less necessary as extractBroodTables() produces a list (dat.list.reduced) with same structure as imported csv files
#write out csv files for each stock:
writeBroodcsv(brood.list = data.list, outpath = ".")

# #compare csv files in two folders:
# diff.list <- compareBroodcsv(path1, path2)
# str(diff.list)
# 



# compare brood tables between years-----------------------------------------

path.new <- settings$data.broodtables.path
path.prior <- "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/workgroups/awg/calibration_model2_analyses/2022/data/matadj/brood_2022-03-21-1413"

filenames.new <- list.files(path=path.new, pattern = "^[A-Z]{3}\\.csv$", full.names = TRUE)
filenames.prior <- list.files(path=path.prior, pattern = "^[A-Z]{3}\\.csv$", full.names = TRUE)


common.filenames <- intersect(basename(filenames.prior), basename(filenames.new))

filenames.new <- paste(path.new, common.filenames, sep = "/" )
filenames.prior <- paste(path.prior, common.filenames, sep = "/" )


importbrood <- function(filenames){
	
 data.long.list <- lapply(filenames, function(x){
 	data.tmp <- data.frame(stock=basename(x), read.csv(x))
 	varying <- colnames(data.tmp)[grep("age[0-9]", colnames(data.tmp))]
 	colnames(data.tmp)[colnames(data.tmp)=="BY"] <- "broodyear"
 	data.long <-  reshape(data.tmp, direction = "long", idvar = c("stock", "broodyear"), varying = list(varying), v.names = "value", times = varying, timevar = 'age')
 	data.long
 	})
 
 do.call(rbind, data.long.list)
}

data.prior <- importbrood(filenames.prior)
data.new <- importbrood(filenames.new)
head(data.prior)
head(data.new)
data.merge <- merge(data.new, data.prior, by=c("stock", "broodyear", "age"))

colnames(data.merge)[colnames(data.merge) == "value.x"] <- "value.new"
colnames(data.merge)[colnames(data.merge) == "value.y"] <- "value.prior"
head(data.merge, 100)
data.merge$diff <- round(data.merge$value.new,0)-round(data.merge$value.prior,0)

knitr::kable(data.merge[data.merge$diff !=0 & !is.na(data.merge$diff),], row.names = F)
