# Antonio Velez-Espino
# May,2019

#This program runs multiple-year ETS model projections for a set of time series data e.g., maturation rates) 
#This program also produces forecasts for as many time units (variable h) as requested by the user


rm(list=ls()) #Clear the workspace

library(forecast)

#-------------------------------------------------------------------------------
#   1. Set specs, paths, etc. and read in the dataset(formatted as in template)
#      ****USER SETTINGS****
#-------------------------------------------------------------------------------
MasterDataFrame<-read.csv(file.choose(),header=TRUE) #direct it to the csv file with the original data (e.g., a CSV version of the MATAEQ file)
fgy <- 1979 # first year of the time series (e.g., 1979 in the MATAEQ files)
lgy <- 2017 # last calendar year without needing forecasting for the youngest age class
            # usually current year(cly) minus 4. For instance, lgy=2006 for calendar year 2010 if 2006 is the last year for the youngest age
            # not affected by inclomplete brood calculations (e.g., application of averages)
            # In some years, lgy=cly-5 instead of cly-4. These two options should be considered when running this code 
cly <- 2021 # calendar year 
h <- 5      # Time horizon for projections of the youngest age from lgy+1 to cly+1
            # For projections through cly+1, h should be 5 when cly-lgy=4 and 6 when cly-lgy=5  
#--------------------------------------------------------------------------------
A.subset <- subset(MasterDataFrame,Year<=lgy)
B.subset <- subset(MasterDataFrame,Year<=(lgy+1))
C.subset <- subset(MasterDataFrame,Year<=(lgy+2))

stocklist1<-names(MasterDataFrame)[1:length(names(MasterDataFrame))] #vector of pop'n names including year
stocklist2<-names(MasterDataFrame)[2:length(names(MasterDataFrame))] #vector of pop'n names excluding year

#-------------------------------------------------------------------------------------------
#   2. Application of ets forecasting to multiple time series in the input file (MasterDataFrame)
#-------------------------------------------------------------------------------------------

# First projection applied to first-age time series ending at cly-4  (OR cly-5)

TimeSeries1 <- ts(A.subset[,-1], start=fgy, end=lgy, frequency=1) #select the time series start and end for first projection
ns <- ncol(TimeSeries1)
fcast1 <- matrix(NA,nrow=h,ncol=ns,dimnames = list(NULL, stocklist2))
for(i in 1:ns)
  fcast1[,i] <- forecast(TimeSeries1[,i], h=h)$mean
  
# Constraining projected values to the range [0,1] for projections cly-3:cly+1(OR cly-4:cly+1)
  df1 <- data.frame(fcast1)  
  m1 <- as.matrix(df1)
  m1[m1<0] <- 0
  m1[m1>1] <- 1
  df1 <- as.data.frame(m1)
                                   
Years1 <- data.frame(Year=(lgy+1):(lgy+h))

dfy1 <- cbind(Years1,df1)

### Appending projected values
for(j in 1:h)
MasterDataFrame[MasterDataFrame$Year==(lgy+j),c('BON1','CBC1','CWF1','FCF1','FHF1','FS21','FSO1','LGS1','LRW1',
                                  'MCB1','MGS1','MOC1','NBC1','NOC1','NSA1','SKG1','SPR1','SSA1','SUM1','TST1',
                                  'UGS1','URB1','WCH1','WCN1','WSH1','WVH1','WVN1')]<-dfy1[dfy1$Year==(lgy+j),
                                c('BON1','CBC1','CWF1','FCF1','FHF1','FS21','FSO1','LGS1','LRW1',
                                  'MCB1','MGS1','MOC1','NBC1','NOC1','NSA1','SKG1','SPR1','SSA1','SUM1','TST1',
                                  'UGS1','URB1','WCH1','WCN1','WSH1','WVH1','WVN1')]  

###############################################################################

# Second projection applied to second-age time series ending at cly-3  (OR cly-4)

TimeSeries2 <- ts(B.subset[,-1], start=fgy, end=(lgy+1), frequency=1) #select the time series start and end for second projection 

ns <- ncol(TimeSeries2)
fcast2 <- matrix(NA,nrow=h-1,ncol=ns,dimnames = list(NULL, stocklist2))
for(i in 1:ns)
  fcast2[,i] <- forecast(TimeSeries2[,i], h=h-1)$mean
  
# Constraining projected values to the range [0,1] for projections cly-2:cly+1(OR cly-3:cly+1)
  df2 <- data.frame(fcast2)  
  m2 <- as.matrix(df2)
  m2[m2<0] <- 0
  m2[m2>1] <- 1
  df2 <- as.data.frame(m2) 

Years2 <- data.frame(Year=(lgy+2):(lgy+h))

dfy2 <- cbind(Years2,df2)

### Appending projected values
for(k in 2:h)
MasterDataFrame[MasterDataFrame$Year==(lgy+k),c('BON2','CBC2','CWF2','FCF2','FHF2','FS22','FSO2','LGS2','LRW2',
                                                'MCB2','MGS2','MOC2','NBC2','NOC2','NSA2','SKG2','SPR2','SSA2','SUM2','TST2',
                                                'UGS2','URB2','WCH2','WCN2','WSH2','WVH2','WVN2')]<-dfy2[dfy2$Year==(lgy+k),
                                c('BON2','CBC2','CWF2','FCF2','FHF2','FS22','FSO2','LGS2','LRW2',
                                  'MCB2','MGS2','MOC2','NBC2','NOC2','NSA2','SKG2','SPR2','SSA2','SUM2','TST2',
                                  'UGS2','URB2','WCH2','WCN2','WSH2','WVH2','WVN2')]  
                                                                                                                                                                          
###############################################################################

# Third projection applied to third-age time series ending at cly-2  (OR cly-3)

TimeSeries3 <- ts(C.subset[,-1], start=fgy, end=(lgy+2), frequency=1) #select the time series start and end for second projection 

ns <- ncol(TimeSeries3)
fcast3 <- matrix(NA,nrow=h-2,ncol=ns,dimnames = list(NULL, stocklist2))
for(i in 1:ns)
  fcast3[,i] <- forecast(TimeSeries3[,i], h=h-2)$mean
  
# Constraining projected values to the range [0,1] for projections cly-1:cly+1(OR cly-2:cly+1)
  df3 <- data.frame(fcast3)  
  m3 <- as.matrix(df3)
  m3[m3<0] <- 0
  m3[m3>1] <- 1
  df3 <- as.data.frame(m3) 

Years3 <- data.frame(Year=(lgy+3):(lgy+h))

dfy3 <- cbind(Years3,df3)

### Appending projected values
for(p in 3:h)
MasterDataFrame[MasterDataFrame$Year==(lgy+p),c('BON3','CBC3','CWF3','FCF3','FHF3','FS23','FSO3','LGS3','LRW3',
                                                'MCB3','MGS3','MOC3','NBC3','NOC3','NSA3','SKG3','SPR3','SSA3','SUM3','TST3',
                                                'UGS3','URB3','WCH3','WCN3','WSH3','WVH3','WVN3')]<-dfy3[dfy3$Year==(lgy+p),
                                c('BON3','CBC3','CWF3','FCF3','FHF3','FS23','FSO3','LGS3','LRW3',
                                  'MCB3','MGS3','MOC3','NBC3','NOC3','NSA3','SKG3','SPR3','SSA3','SUM3','TST3',
                                  'UGS3','URB3','WCH3','WCN3','WSH3','WVH3','WVN3')]  
            
                                                                                  
A1.Output <- subset(MasterDataFrame,Year<=(lgy+h-1))

B1.Output <- subset(MasterDataFrame,Year<=(lgy+h))   
                      
                                                                                                                                                                      
#-------------------------------------------------------------------------------------------
#   3. Writing the output into csv files 
#-------------------------------------------------------------------------------------------
# Two csv output files are produced:(1)One with projections up to current year cly; and, (2) one with projections up to year cly+1
# The first file can be used for projections of cly+1 using other methodologies (e.g., trend analysis)

#Create an output data frame (set number of rows to number of pops) 
Output_path_string <- file.path(choose.dir()) #Select the place to write the output file 
numcol<- length(stocklist1)
numraw<- cly-fgy+2
 
OutputSubset1 <- as.data.frame(
  matrix(nrow = numraw, ncol = numcol,
         dimnames = list(NULL, stocklist1))) 
         
OutputSubset1 <- A1.Output 


OutputSubset2 <- as.data.frame(
  matrix(nrow = numraw, ncol = numcol,
         dimnames = list(NULL, stocklist1))) 
         
OutputSubset2 <- B1.Output
  

stamp<-format(Sys.time(), "%a %b %d") #DateStampForOutputFile


# write.csv(OutputSubset1,file = paste(Output_path_string,"\\",stamp,"ETS_MR_Forecasts_to_calendar_year_hn.csv",sep=""),row.names=FALSE)
write.csv(OutputSubset2,file = paste(Output_path_string,"\\",stamp,"ETS_MR_Forecasts_one_year_in_the_future_hn.csv",sep=""),row.names=FALSE)


############################ This is the END ##############################
