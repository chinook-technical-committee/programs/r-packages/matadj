
install.packages("remotes")
remotes::install_git("https://gitlab.com/chinook-technical-committee/programs/r-packages/matadj.git", build_vignettes = TRUE, force = TRUE) 


library(matadj)
library(arsenal)

user_settings<-yaml::read_yaml("user.settings.config")
stock_data<-load_stocks() 



##### Comparing HRJs from ream and coshak 
HRJ_BY_ream <- load_hrjs(hrjfilepath=paste0(user_settings$path.data.tmp, "REAM_out"))
HRJ_BY_ream_URB<-HRJ_BY_ream %>% filter(StockID=="URB") %>% arrange(data.type, broodyear,fishery, returnyear, age.true)


HRJ_BY_coshak <- load_hrjs(hrjfilepath=paste0(user_settings$path.data.tmp, "COSHAK_OUT_2023"))
HRJ_BY_coshak_URB<-HRJ_BY_coshak %>% filter(StockID=="URB") %>% arrange(data.type, broodyear,fishery, returnyear, age.true)


all.equal(HRJ_BY_ream_URB,   HRJ_BY_coshak_URB)

## comparing using comparedf
summary(arsenal::comparedf(HRJ_BY_ream_URB, HRJ_BY_coshak_URB, control=c(tot.num.val=1, tol.num="absolute")))
cmp<-arsenal::comparedf(HRJ_BY_ream_URB, HRJ_BY_coshak_URB, control=c(tot.num.val=1, tol.num="absolute"))
diffs(cmp)

View(HRJ_BY_ream_URB)
View(HRJ_BY_coshak_URB)

summary(arsenal::comparedf(HRJ_BY_ream_URB, HRJ_BY_coshak_URB,
                           by=c("StockID", "broodyear", "fishery", "data.type", "age.true"), 
                           control=comparedf.control(tot.num.val=1, tol.num="absolute", int.as.num=TRUE)))

cmp<-arsenal::comparedf(HRJ_BY_ream_URB, HRJ_BY_coshak_URB,
                        by=c("StockID", "broodyear", "fishery", "data.type", "age.true"), 
                        control=comparedf.control(tot.num.val=0.01, tol.num="absolute", int.as.num=TRUE))
diffz<-as.data.frame(diffs(cmp))
diffz$values.x<-as.numeric(diffz$values.x)
diffz$values.y<-as.numeric(diffz$values.y)

diffz$abs<-diffz$values.x-diffz$values.y

diffz<-diffz %>% arrange(abs)
View(diffz)