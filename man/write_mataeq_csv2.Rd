% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/export.R
\name{write_mataeq_csv2}
\alias{write_mataeq_csv2}
\title{write_mataeq_csv2}
\usage{
write_mataeq_csv2(longmat)
}
\arguments{
\item{longmat}{A list containing data frames, output of calc_matrate_adj_tim}

\item{outdir}{the path where the matAEW file is to be saved}
}
\description{
write csv input file for MATAEQ program
}
