


# comments ----------------------------------------------------------------
# author: Michael Folkes
# this script will calculate the age-specific 'escapement scaler' for select fraser stocks then, using that scaler, produce the terminal HR values by age. The terminal HR values can be used to expand the escapement values in the fcs file to terminal run and then brood tables can be created.
# There are data gaps in the esc scaler estimates (ages 2 & 5) and those are imputed using a linear regression relating the age-specific scaler to a weighted average of the scaler for the remaining ages from that year brood year. ie age 2 scaler is predicted from weighted avg scaler of ages 3:5.  Then age 5 scaler is predicted from weighted avg scaler of ages 2:4.


# packages ----------------------------------------------------------------
require(ctctools)
require(matadj)
require(utilizer)

# function defs -----------------------------------------------------------

#this is like setdiff, but for data frames
setdiff_df <- function(x,y, idvar=NULL){
  if(is.null(idvar)) idvar <- intersect(names(x), names(y))
  names.x <- names(x)
  x$idvar <- do.call(paste0,x[,idvar])
  y$idvar <- do.call(paste0,y[,idvar])
  
  x[x$idvar %in% setdiff(x$idvar, y$idvar),names.x]
  
}#END setdiff_df



# setup -------------------------------------------------------------------

rm(list=ls())


#user.settings.filepath <- "C:/Users/folkesm/Documents/Projects/salmon/chinook/ctc/workgroups/awg/calibration_model2_analyses/2023/R/user.settings.config"
settings <- yaml::read_yaml(paste0(here::here(), "/user.settings.config"))
settings



stockmap <- matadj::defs$stockmap

fishery.def <- ctctools::defs$fisherymapping
fishery.def <- fishery.def[!is.na(fishery.def$fisherymodel49) & fishery.def$fisherymodel49name != "ESCAPEMENT" ,]
fishery.def <- fishery.def[,c("gear", "fisherymodel49", "fisherymodel49name", "locale",  "fisheryera80", "fisheryera80name", "Cohort_Type", "Term_PreTerm2", "Term_PreTerm3", "Term_PreTerm4", "Term_PreTerm5")]


#update the forecast year in the fcs file
fcs.forecast.year <- 2024

# this list identifies what stock-ages are to have models fit to impute escapement scalers
forecast.list <- list(
  FHF=list(
    models=list(
      list(
        age.predict=2,
        age.covariate=3:5),
      
      list(
        age.predict=2,
        age.covariate=3:4),
      
      list(
        age.predict=5,
        age.covariate=2:4),
      
      list(
        age.predict=5,
        age.covariate=3:4)
    )#models
  ),#stock
  
  FCF=list(
    models=list(
      list(
        age.predict=2,
        age.covariate=3:5),
      
      list(
        age.predict=2,
        age.covariate=3:4),
      
      list(
        age.predict=5,
        age.covariate=2:4),
      
      list(
        age.predict=5,
        age.covariate=3:4)
    )#models
  ),#stock
  
  FSO=list(
    models=list(
    list(
    age.predict=2,
    age.covariate=3:5),
    
    list(
    age.predict=2,
    age.covariate=3:4),
    
    list(
    age.predict=5,
    age.covariate=2:4),
    
    list(
    age.predict=5,
    age.covariate=3:4)
   )#models
)#stock
)

forecast.list <- Map(c, stockmodel=names(forecast.list), forecast.list)
forecast.list <- Map(c, forecast.list, fcs.forecast.year= fcs.forecast.year)

#for now, exlude FSO as it lacks 4 ages in the fcs file
forecast.list <- forecast.list[names(forecast.list) != "FSO"]


# import raw data -----------------------------------------------------------

# fcs file
filepaths <- list.files(settings$data.path, "fcs$", ignore.case = TRUE, full.names = TRUE)

# I have several fcs files, in my case, choosing the 3rd in the vector 
filepaths <- filepaths[2]
data.fcs <- matadj::readFCS2(filepaths)
data.fcs <- data.fcs$data.long
colnames(data.fcs)[colnames(data.fcs)=="stock"] <- "stockmodel"



# loop through statistical models and impute esc scalers ----------------------


#notice I'm not running FSO (forecast.list[3]) due to it lacking 4 ages in the fcs file.
res <- lapply(forecast.list, function(stockmodel){
  
  #import hrjs
  args.df <- stockmap[stockmap$stockmodel %in% stockmodel$stockmodel,]
  
  hrj.filepaths <- list.files(settings$data.hrj.path, pattern =  "hrj$", ignore.case = TRUE, full.names = TRUE, recursive = TRUE)
  search.term <- paste(paste0(args.df$stockera))
  filename <- hrj.filepaths[grepl(search.term, hrj.filepaths, ignore.case = TRUE)]
  
  data.hrj.list <- ctctools::readHRJtext(filename)
  
  # data:hrj esc
  hrj.esc.long <- reshapeHRJtolong.basic(data.hrj.list$hrj.esc.list)
  hrj.esc.long <- hrj.esc.long$HRJ_BY[hrj.esc.long$HRJ_BY$data.type=="escapement",]
  colnames(hrj.esc.long)[colnames(hrj.esc.long)=="brood"] <- "brood.year"
  colnames(hrj.esc.long)[colnames(hrj.esc.long)=="StockID"] <- "stockera"
  colnames(hrj.esc.long)[colnames(hrj.esc.long)=="value"] <- "value.hrj"
  
  data.hrj <- merge(hrj.esc.long, args.df[,c("stockmodel", "stockera")], by = "stockera")
  
  # data: hrj catch
  hrj.cwt.long <- reshapeHRJtolong.basic(data.hrj.list$hrj.cwt.list)
  hrj.cwt.long <- hrj.cwt.long$HRJ_BY
  names(hrj.cwt.long)[names(hrj.cwt.long)=="StockID"] <- "stockera"
  names(hrj.cwt.long)[names(hrj.cwt.long)=="fisheryindex"] <- "fisheryera80"
  
  hrj.cwt.long <- merge(hrj.cwt.long, args.df[,c("stockmodel", "stockera")], by = "stockera")
  
  varying <- colnames(fishery.def)[grep("Term_PreTerm[0-9]",colnames(fishery.def))]
  cols.keep <- c('gear', 'fisherymodel49', 'fisherymodel49name', 'fisheryera80', 'fisheryera80name', "Cohort_Type", 'Term_PreTerm2', 'Term_PreTerm3', 'Term_PreTerm4', 'Term_PreTerm5')
  
  idvar <- setdiff(cols.keep, varying)
  fishery.def.long <- reshape(fishery.def[,cols.keep], direction = "long", idvar=idvar, varying=list(varying), timevar = "age", times = gsub("Term_PreTerm", "", varying), v.names = "Term_Preterm_byage")
  row.names(fishery.def.long) <- NULL
  
  hrj.cwt.long <- merge(hrj.cwt.long, fishery.def.long, by=c("fisheryera80", "age"))
  hrj.cwt.long$Term_PreTerm.tmp <- hrj.cwt.long$Term_Preterm_byage
  
  #fraser.term.fisheries <- unique(fishery.def$fisheryera80name)[grep("^TFR", unique(fishery.def$fisheryera80name))]
  
  data.hrj.TMNOM.total <- aggregate(value~stockera+broodyear+age+data.type+Term_PreTerm.tmp, data=hrj.cwt.long[hrj.cwt.long$data.type=="NomTot" & hrj.cwt.long$Term_PreTerm.tmp=="TERMINAL" ,], sum)
  
  # data.hrj.TMNOM.total.wide <- reshape(data.hrj.TMNOM.total[,c("stockera", "broodyear", "age","data.type",  "Term_PreTerm.tmp", "value")], direction = 'wide', idvar=c("stockera", "broodyear", "data.type","Term_PreTerm.tmp"), timevar = c("age"))
  # data.hrj.TMNOM.total.wide <- merge(data.frame(broodyear=1981:2020), data.hrj.TMNOM.total.wide, all.x=TRUE)
  # data.hrj.TMNOM.total.wide <- data.hrj.TMNOM.total.wide[order(data.hrj.TMNOM.total.wide$broodyear),]
  
  if(stockmodel$stockmodel=="FCF"){
    data.hrj.TMNOM.TFRASERFS <- hrj.cwt.long[hrj.cwt.long$stockmodel=="FCF" & hrj.cwt.long$data.type=="NomTot" & hrj.cwt.long$fisheryera80name=="TFRASER FS",c("stockera", "broodyear", "age", "value")]
    
    data.wide <- reshape(data.hrj.TMNOM.TFRASERFS[order(data.hrj.TMNOM.TFRASERFS$broodyear, data.hrj.TMNOM.TFRASERFS$age),], direction = 'wide', idvar = c("stockera", "broodyear"), timevar = "age" )
    intersect(names(data.hrj), names(data.hrj.TMNOM.TFRASERFS))
    data.hrj.tmp <- merge(data.hrj, data.hrj.TMNOM.TFRASERFS, all.x = TRUE)
    data.hrj.tmp$value.hrj[! is.na(data.hrj.tmp$value)] <- data.hrj.tmp$value.hrj[! is.na(data.hrj.tmp$value)] + data.hrj.tmp$value[! is.na(data.hrj.tmp$value)]
    data.hrj <- data.hrj.tmp[,colnames(data.hrj)]
    
  } else{
    data.hrj.TMNOM.TFRASERFS <- NULL
  }#END if(stockmodel=="FCF"){
  
  
  #subset fcs to just this single model stock
  data.fcs <- data.fcs[data.fcs$stockmodel %in% args.df$stockmodel & data.fcs$age_structure==1,]
  data.fcs <- merge(stockmap[,c("stockmodel", "stockera", "agestart", "matadj")],data.fcs, by="stockmodel")
 
  data.fcs$age <- as.integer(substr(data.fcs$age,4,4))-2+data.fcs$agestart
  data.fcs$calendaryear <- data.fcs$calendar_year+1900
  data.fcs <- data.fcs[data.fcs$calendaryear < stockmodel$fcs.forecast.year,]
  data.fcs$broodyear <- data.fcs$calendaryear-data.fcs$age
  colnames(data.fcs)[colnames(data.fcs)=="value"] <- "value.fcs"
  
  data.all <- merge(data.fcs[,c("stockmodel", "broodyear", "age", "value.fcs")], data.hrj[,c("stockmodel", "broodyear", "age", "value.hrj")])
  
  data.all$esc.scaler <- data.all$value.fcs/data.all$value.hrj
  data.all$estimate.method[! (is.na(data.all$esc.scaler)| is.infinite(data.all$esc.scaler))] <- "direct"
  
  
  # model fitting
  modelfits <- lapply(stockmodel$models, function(model){
  
  #data.sub subsets to ages needed in dependent or independent vars
  data.sub <- data.all[data.all$age %in% c(model$age.predict, model$age.covariate),]
  
  years.forecast <- data.sub$broodyear[(data.sub$age == model$age.predict & data.sub$value.hrj<=1) | (data.sub$age == model$age.predict & is.na(data.sub$value.fcs)) | (data.sub$age == model$age.predict & data.sub$value.fcs<=1)]
  #this is the start of vector of years to exclude from regression fitting, but don't do it yet as some year-ages don't need forecasting
  years.exclude <- data.sub$broodyear[data.sub$value.fcs<=1 | data.sub$value.hrj<=1]
  years.exclude <- years.exclude[!is.na(years.exclude)]
  
  data.sub$variable[data.sub$age==model$age.predict] <- "dependent"
  data.sub$variable[data.sub$age %in% model$age.covariate] <- "covariate"
  
  data.sub.sums <- aggregate(.~stockmodel+broodyear+variable, data=data.sub[ c("stockmodel", "broodyear", "variable", "value.fcs", "value.hrj")], sum)
  data.sub.sums$scaler <- data.sub.sums$value.fcs/data.sub.sums$value.hrj
  
  idvar <-  c("stockmodel", "broodyear")
  varying <- "variable"
  cols.keep <- c(idvar, varying, "scaler")
  
  data.sub.sums.wide <- reshape(data.sub.sums[, cols.keep], direction = 'wide', idvar = idvar, timevar = varying)
  data.sub.sums.wide <- data.sub.sums.wide[order(data.sub.sums.wide$stockmodel, data.sub.sums.wide$broodyear),]
  
  #if dependent or covariate scalers are beyond their 95% quantile then exclude and forecast that year
  scaler.dependent.limit <- quantile(data.sub.sums.wide$scaler.dependent[! is.infinite(data.sub.sums.wide$scaler.dependent) & (! data.sub.sums.wide$broodyear %in% years.exclude)], probs = 0.95, na.rm = TRUE)
  scaler.covariate.limit <- quantile(data.sub.sums.wide$scaler.covariate[! is.infinite(data.sub.sums.wide$scaler.dependent) & (! data.sub.sums.wide$broodyear %in% years.exclude)], probs = 0.95, na.rm = TRUE)
  
  years.fit <- data.sub.sums.wide$broodyear[data.sub.sums.wide$scaler.dependent <= scaler.dependent.limit & data.sub.sums.wide$scaler.covariate <= scaler.covariate.limit]
  years.fit <- years.fit[! is.na(years.fit)]
  
  years.fit <- setdiff(years.fit, years.exclude)
  data.fitting <- data.sub.sums.wide[data.sub.sums.wide$broodyear %in% years.fit,]
 
  lmfit <- lm(scaler.dependent~scaler.covariate, data=data.fitting)
  
  
  newdata <- data.sub.sums.wide[data.sub.sums.wide$broodyear %in% years.forecast,]
  
  if(nrow(newdata)>0){
    esc.scaler <- predict(lmfit, newdata=newdata)
    estimate.method <- paste0("regression_", paste0(length(model$age.covariate), "ages"))
    output <- data.frame(stockmodel=newdata$stockmodel, broodyear=newdata$broodyear, age=model$age.predict,  esc.scaler=esc.scaler, estimate.method= estimate.method)
    data.all[data.all$stockmodel %in% output$stockmodel & data.all$broodyear %in% output$broodyear & data.all$age %in% output$age, c("esc.scaler", "estimate.method")] <- output[, c("esc.scaler", "estimate.method")]
  }
  
  return(data.all)
  })#END lapply(stockmodel$models, function(model){
  
  data.all <- rbind(modelfits[[1]][modelfits[[1]]$age !=5,], modelfits[[3]][modelfits[[3]]$age ==5,])
  
  return(list(stockmodel=stockmodel$stockmodel, data.all=data.all, data.hrj.TMNOM.total=data.hrj.TMNOM.total, data.hrj.TMNOM.TFRASERFS=data.hrj.TMNOM.TFRASERFS, modelfits=modelfits))
  
})#END lapply




# model fit summary -------------------------------------------------------

# 
# lapply(model.fits, function(x)lapply(x,summary))
# 
# lapply(model.fits, function(x)lapply(x,function(x2){
# 		lmtest::bgtest(x2)}))
# 
# lmtest::bgtest(model.fits$age5predict$covariate_2age)
# 
# 
# #https://stats.stackexchange.com/questions/61332/comparing-aic-of-a-model-and-its-log-transformed-version
# 
# lapply(model.fits, function(x){
# 	lapply(x,function(covare){
# 		if(any(!is.na(covare))) AIC(covare)
# 					#	if(any(!is.na(covare))) AIC(covare)+sum(2*covare$model$esc.scaler.y)
# 	})
# 	})



# model diagnostics -------------------------------------------------------

# dev.new()
# plot(model.fits$age2predict$covariate_3age$fitted.values, model.fits$age2predict$covariate_3age$residuals)
# 
# lapply(names(model.fits), function(age.predict){
# 	lapply(names(model.fits[[age.predict]]), function(covariate){
# 		print(c(age.predict, covariate))
# 		if(! is.na(model.fits[[age.predict]][[covariate]])){
# 
# 
# 		filename <- paste("regresssion_diagnostics", stockmodel, "age", age.predict, covariate, "non-transformed", ".png", sep="_")
# 	png(filename, width=8, height=8, units='in', res=300)
# 
# 		layout(matrix(1:4, ncol = 2))
# 		par(mar=c(4,4,4,4))
# 		plot((model.fits[[age.predict]][[covariate]]$model$esc.scaler.covariate), (model.fits[[age.predict]][[covariate]]$model$esc.scaler.y), xlab = "covariate", ylab="y")
# 		title(paste(stockmodel, "age", age.predict, covariate))
# 		#abline(model.fits[[age.predict]][[covariate]])
# 		#browser()
# 		newdata <- model.fits[[age.predict]][[covariate]]$model[order(model.fits[[age.predict]][[covariate]]$model$esc.scaler.covariate),]
# 		y.pred <- predict(model.fits[[age.predict]][[covariate]], newdata)
# 		lines((newdata$esc.scaler.covariate), (y.pred))
# 
# 		plot(model.fits[[age.predict]][[covariate]], ask = FALSE, which = 1:3)
# 
# 		layout(1)
# 		dev.off()
# 		}
# 	})
# })


# summarize results -------------------------------------------------------


# #colnames(res)[colnames(res)=="esc.scaler"] <- "esc.estimated"
# data.all[is.na(data.all$esc.scaler) | is.infinite(data.all$esc.scaler),]
# data.all[data.all$brood.year==2006,]
# 
# intersect(names(data.all), names(res))
# data.all <- merge(data.all, res, all.x = TRUE)
# data.all[!is.na(data.all$esc.estimated),]
# data.all$variables[is.na(data.all$variables)] <- 1
# data.all$esc.scaler[!is.na(data.all$esc.estimated)] <- data.all$esc.estimated[!is.na(data.all$esc.estimated)]
# data.all[data.all$variables %in% 2:3,]
# data.all$esc.scaler <- round(data.all$esc.scaler)
# 
# data.all.wide <- reshape(data.all, direction = "wide", idvar = c("stockmodel", "brood.year"), timevar = "age", drop=c("value.fcs", "value.hrj", "variables", "esc.estimated"))
# data.all.wide <- merge(data.frame(brood.year=1981:2020), data.all.wide, all.x=TRUE)
# write.csv(data.all.wide, file="escapement_scaler_regressions_estimates.csv", row.names=FALSE)
# matplot(data.all.wide$brood.year, y=data.all.wide[,3:6], type="l")
# matpoints(data.escscaler.chuck$brood.year, data.escscaler.chuck[,1:4], pch=1)
# 
# 
# data.escscaler.chuck.long$estimate.method <- "expansion"
# require(ggplot2)
# dev.new()
# ggplot(data = data.all,  aes(brood.year, esc.scaler))+
# 	geom_line()+geom_point(size=1, aes(col=as.factor(estimate.method)))+
# 	geom_point(data=data.escscaler.chuck.long,  aes(brood.year, esc.scaler), shape=1, size=2)+
# 	facet_wrap(vars(age), scales = "free_y")
# filename <- "escapement_scaler_FCF_compare_regression_parken.png"
# ggsave(filename, height = 8, width = 15, dpi=400)
# shell.exec(filename)
# 
# 
# fishery.def <- getFisherydef()
# str(fishery.def)
# 
# 
# 
# 
# 
# hrj.xls.pivot.long <- reshape(hrj.xls.pivot, direction = "long", idvar = "brood.year", varying = list(2:5), timevar = "age", times=2:5, v.names="pivot")
# 
# data.tmp <- merge(hrj.cwt.sums[hrj.cwt.sums$Term_PreTerm.tmp=="T",], hrj.xls.pivot.long, by=c("brood.year", "age"))
# data.tmp$diff <- data.tmp$value-data.tmp$pivot
# tail(data.tmp, 50)
# 
# #These data are part of the 'True terminal mort' variable, but the associated xls columns are empty.
# data.hrj.TMNOM.TFRASERFS <- hrj.cwt[hrj.cwt$data.type=="NomTot" & hrj.cwt$FisheryName=="TFRASER FS",c("era_stock", "brood.year", "age", "value")]
# data.hrj.TMNOM.TFRASERFS.wide <- reshape(data.hrj.TMNOM.TFRASERFS, direction = "wide", idvar = "brood.year", timevar = "age")
# data.hrj.TMNOM.TFRASERFS.wide <- merge(data.frame(brood.year=1981:2020), data.hrj.TMNOM.TFRASERFS.wide, all.x=TRUE, sort = TRUE)
# write.csv(data.hrj.TMNOM.TFRASERFS.wide, file = "hrj_tfraserFS.csv", row.names = F)





# compare esc scaler with xls file estimates -----------------------------------------

# this is only needed for comparison to the R output
# data.index <- read_indextable(paste(settings$data.matadj.path, "table_index.xlsx", sep="/"))
# 
# data.index$filename <- paste(settings$data.matadj.path, data.index$filename, sep="/")
# data.fraser <- import_excelranges(data.index = data.index)
# 
# 
# 
# data.escscaler.chuck <- data.fraser$`Harrison Cohort Check Mar 202.xlsx`$FCSDataForCalculations$Escapement_Scalar_$data
# data.escscaler.chuck <- data.fraser$`Chilliwack Cohort Check Mar 2023.xlsx`$FCSDataForCalculations$`Escapement_+_Sport_Scalar_`$data
# 
# data.escscaler.chuck <- data.fraser$`FSO Cohort Check Mar 2023.xlsx`$FCSDataForCalculations$Escapement_Scalar_$data
# 
# data.escscaler.chuck$broodyear <- 1975+1:nrow(data.escscaler.chuck)
# 
# data.escscaler.chuck.long <- reshape(data.escscaler.chuck, direction = "long", idvar = "brood.year", varying = list(1:4), timevar = "age", v.names = "esc.scaler")
# data.escscaler.chuck.long$age <- data.escscaler.chuck.long$age+1
# data.escscaler.chuck.long$stockmodel <- "FCF"
# data.escscaler.chuck.long$estimate.method <- "Parken"


# calc terminal HR --------------------------------------------------------


lapply(res, function(stockmodel){
  
  data.all <- stockmodel$data.all
  data.hrj.TMNOM.total <- stockmodel$data.hrj.TMNOM.total
  data.hrj.TMNOM.TFRASERFS <- stockmodel$data.hrj.TMNOM.TFRASERFS
  names(data.hrj.TMNOM.total)[names(data.hrj.TMNOM.total)=="value"] <- "TMNOM.total"
  
  
  # ggplot(data.all, aes(broodyear, esc.scaler, col=estimate.method))+
  #   geom_line(data=data.all, aes(broodyear, esc.scaler), col='grey')+
  #   geom_point(shape=16, size=1)+
  #   geom_point(data=data.escscaler.chuck.long, shape=1, size=3)+
  #   facet_wrap(vars(age), ncol = 1, scales = "free_y")
  # 
  
  
  if(!is.null(data.hrj.TMNOM.TFRASERFS)) {
    names(data.hrj.TMNOM.TFRASERFS)[names(data.hrj.TMNOM.TFRASERFS)=="value"] <- "TMNOM.TFRASERFS"
    data.terminalhr <- merge(data.hrj.TMNOM.total, data.hrj.TMNOM.TFRASERFS)
  } else {
    data.terminalhr <- data.hrj.TMNOM.total
    }
  
  data.terminalhr <- merge(data.terminalhr, stockmap[,c("stockmodel", "era_stock")])
  data.terminalhr <- merge(data.all, data.terminalhr)
  
  if(stockmodel$stockmodel =="FCF"){
  	data.terminalhr$hr <- ((data.terminalhr$TMNOM.total-data.terminalhr$TMNOM.TFRASERFS)*data.terminalhr$esc.scaler) / ((data.terminalhr$TMNOM.total-data.terminalhr$TMNOM.TFRASERFS)*data.terminalhr$esc.scaler+data.terminalhr$value.fcs)
  }else{
  	data.terminalhr$hr <- ((data.terminalhr$TMNOM.total)*data.terminalhr$esc.scaler) / ((data.terminalhr$TMNOM.total)*data.terminalhr$esc.scaler+data.terminalhr$value.fcs)
  }
  
  data.terminalhr$calendar_year <- data.terminalhr$broodyear+data.terminalhr$age
  data.terminalhr$comment <- "estimated N Brown; 14-mar-2024; esc scaler using regression method"
  #View(data.terminalhr[order(data.terminalhr$calendar_year, data.terminalhr$age),c("stockmodel", "calendar_year", "age", "hr", "comment")])
  
  filename <- paste("terminal_HR", stockmodel$stockmodel, Sys.Date(), ".csv", sep="_")
  filepath <- paste(settings$data.matadj.path, filename, sep="/")
  write.csv(data.terminalhr[,c("stockmodel", "calendar_year", "age", "hr", "comment")],file =  filepath, row.names = FALSE)
})

# merge with Tim's Columbia data ---------------------------------------------------

#when stored on sharepoint the file becomes an xls so might as well read in that file:
filepath <- list.files(settings$data.matadj.path, "terminalHR_age_specific.*csv", full.names = TRUE)
filepath
#hr.columb <- importTerminalHR(filepath)
hr.columb <- read.csv(filepath)
#colnames(hr.columb)[colnames(hr.columb)=="model_stock"] <- "stockmodel"
#hr.columb <- hr.columb[hr.columb$stockmodel %in% c("NOC", "MOC"),]
str(hr.columb)

filepath <- list.files(settings$data.matadj.path, "terminal.*2024-03-14.*csv", full.names = TRUE)
filepath
hr.fraser <- do.call(rbind, lapply(filepath, read.csv))
str(hr.fraser)
colnames(hr.fraser)[colnames(hr.fraser)=="stockmodel"] <- "model_stock"

library(openxlsx)

hr <- rbind(hr.fraser, hr.columb)
hr <- hr[order(hr$model_stock, hr$calendar_year, hr$age),]
 str(hr)
 filename <- paste("terminalHR", format(Sys.time(), "%Y-%m-%d-%H-%M"), ".xlsx", sep="_")
 filepath <- paste(settings$data.matadj.path, filename, sep="/")
 write.xlsx(x = hr,filepath)

# ggplot(hr, aes(calendar_year, hr))+
# 	geom_line()+geom_point()+
# 	facet_grid(vars(age), vars(stockmodel), scales = "free_y")
# filename <- "terminal_harvest_rate_series.png"
# ggsave(filename, height = 8, width = 15, dpi=400)
# shell.exec(filename)

